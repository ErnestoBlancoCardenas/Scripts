param( [string] $BaseFileName, [string] $ChangedFileName )

[string] $ErrorActionPreference = 'Stop'
[string] $BaseFileName = (Resolve-Path $BaseFileName).Path
[string] $ChangedFileName = (Resolve-Path $ChangedFileName).Path
 
(Get-ChildItem $BaseFileName).IsReadOnly = $false;

try 
{
    $word = New-Object -ComObject Word.Application
    $document = $word.Documents.Open($BaseFileName)
    $document.Compare( $ChangedFileName, [ref] "Comparison", [ref] 2, [ref] $true, [ref] $true)
    $word.Visible = $true
    $word.ActiveDocument.Saved = 1
    $document.Close( [ref] 0 )
} 
catch 
{
    Add-Type -AssemblyName System.Windows.Forms
    [System.Windows.Forms.MessageBox]::Show($_.Exception)
}

<#
Git Integration:

1- Open or create a .gitattributes file in the repo root directory. Add the following line:
*.docx diff=word

2- Open or create a .gitconfig file in the home directory(global gitconfig). Add the following line:
[diff "word"]
  command = call_the_script
#>
