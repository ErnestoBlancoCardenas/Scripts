// ==UserScript==
// @name                MicrosoftDocsSwitchingLanguage
// @include               http*://docs.microsoft.com/*
// ==/UserScript==

var switcher = document.createElement('li');
switcher.innerHTML = '<Button>[Deutsch--English]</Button>';
var actionList = document.querySelector('.action-list');
actionList.insertBefore(switcher, actionList.firstElementChild);

switcher.firstElementChild.addEventListener('click', function () {
  if (document.URL.search(/\/en-us\//) != -1) {
    window.location.replace(location.href.replace(/\/en-us\//, '\/de-de\/'));
  }
  if (document.URL.search(/\/de-de\//) != -1) {
    window.location.replace(location.href.replace(/\/de-de\//, '\/en-us\/'));
  }
}, false);
